# README #

| Host  | develop | beta | master |
|-------|---------|------|--------|
| MyGet | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech-develop?identifier=b44c9a52-b7ea-4bdd-9883-ba2be97c32e6)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech-beta?identifier=56e07f92-61c2-45cb-9364-366e180f3262)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech?identifier=324342cd-543a-4f2d-8bfe-d26e2f2b806c)](https://www.myget.org/gallery/acrotech) |

## About ##

This project provides a common extensions library that supplies a `Get*OrDefault` function pattern for `IServiceProvider` and `IServiceLocator`. These extension methods will simply absorb an `ActivationException` that is caused by a missing type registration, and instead return a provided default value. This project also provides some adapters, as listed below.

## Adapters ##

This project also contains some `IServiceLocator` adapters. All adapters simply sublcass `ServiceLocatorImplBase` and override `DoGetAllInstances` and `DoGetInstance` (this is the standard pattern for `Microsoft.Practices.ServiceLocation`).

The following Adapters are avaiable from this project:

* `SimpleInjector`

See adapter `README` file for more information on the adapter.

### Simple Injector ###

This adapter was created to provide a proper universally portable CommonServiceLocator implementation. The official Simple Injector adapter does not support profile 344 and depends upon a custom PCL build of the CommonServiceLocator.
