Prep-Project (Get-Path "Extensions")
Prep-Project (Get-Path "Adapters\SimpleInjector")

Build (Get-Path "Extensions") "Extensions.csproj"
Build (Get-Path "Adapters\SimpleInjector") "CommonServiceLocator.SimpleInjector.csproj"

Build-Project (Get-Path "Test\Test.csproj")
Build-Project (Get-Path "Adapters\SimpleInjector.Test\SimpleInjector.Test.csproj")

Run-Tests (Get-Path "Test\bin\$env:Configuration\Acrotech.CommonServiceLocator.Test.dll")
Run-Tests (Get-Path "Adapters\SimpleInjector.Test\bin\$env:Configuration\Acrotech.CommonServiceLocator.SimpleInjector.Test.dll")

# Until NuGet 3.0.0 is released (and deployed to MyGet and other build services), we need to use the patched NuGet stored locally.
# this is a workaround to packing PCL libraries with replacement tokens in the nuspec
$env:NuGetExe = (Get-Path ".nuget\NuGet.exe")
Log-Build "Updated NuGetExe to $env:NuGetExe"

Pack-Packagable (Get-Path "Extensions\Extensions.csproj")
Pack-Packagable (Get-Path "Adapters\SimpleInjector\CommonServiceLocator.SimpleInjector.csproj")
