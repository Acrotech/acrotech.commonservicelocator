﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.ServiceLocation;
using System.Diagnostics.CodeAnalysis;

namespace Acrotech.CommonServiceLocator.Test
{
    [TestClass]
    public class UnitTestServiceLocatorTest
    {
        [TestMethod]
        public void ThrowActivationExceptionIfNullResultTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = "test";

            // ensure no exceptions occur
            locator.ThrowActivationExceptionIfNullResult();
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void ThrowActivationExceptionIfNullResultWithNullResultTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.ThrowActivationExceptionIfNullResult();
        }

        [TestMethod]
        public void GetAllInstancesGenericTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = new[] { "test" };
            var result = locator.GetAllInstances<string>();

            Assert.AreEqual(locator.NextResult, result);
            Assert.AreEqual(typeof(string), locator.LastServiceType);
            Assert.IsNull(locator.LastKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void GetAllInstancesGenericWithNullResultTest()
        {
            new UnitTestServiceLocator().GetAllInstances<string>();
        }

        [TestMethod]
        public void GetAllInstancesTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = new[] { "test" };
            var result = locator.GetAllInstances(typeof(string));

            Assert.AreEqual(locator.NextResult, result);
            Assert.AreEqual(typeof(string), locator.LastServiceType);
            Assert.IsNull(locator.LastKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void GetAllInstancesWithNullResultTest()
        {
            new UnitTestServiceLocator().GetAllInstances(typeof(string));
        }

        [TestMethod]
        public void GetInstanceGenericWithKeyTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = "test";
            var result = locator.GetInstance<string>("key");

            Assert.AreEqual(locator.NextResult, result);
            Assert.AreEqual(typeof(string), locator.LastServiceType);
            Assert.AreEqual("key", locator.LastKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void GetInstanceGenericWithKeyWithNullResultTest()
        {
            new UnitTestServiceLocator().GetInstance<string>("key");
        }

        [TestMethod]
        public void GetInstanceGenericTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = "test";
            var result = locator.GetInstance<string>();

            Assert.AreEqual(locator.NextResult, result);
            Assert.AreEqual(typeof(string), locator.LastServiceType);
            Assert.IsNull(locator.LastKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void GetInstanceGenericWithNullResultTest()
        {
            new UnitTestServiceLocator().GetInstance<string>();
        }

        [TestMethod]
        public void GetInstanceWithKeyTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = "test";
            var result = locator.GetInstance(typeof(string), "key");

            Assert.AreEqual(locator.NextResult, result);
            Assert.AreEqual(typeof(string), locator.LastServiceType);
            Assert.AreEqual("key", locator.LastKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void GetInstanceWithKeyWithNullResultTest()
        {
            new UnitTestServiceLocator().GetInstance(typeof(string), "key");
        }

        [TestMethod]
        public void GetInstanceTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = "test";
            var result = locator.GetInstance(typeof(string));

            Assert.AreEqual(locator.NextResult, result);
            Assert.AreEqual(typeof(string), locator.LastServiceType);
            Assert.IsNull(locator.LastKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void GetInstanceWithNullResultTest()
        {
            new UnitTestServiceLocator().GetInstance(typeof(string));
        }

        [TestMethod]
        public void GetServiceTest()
        {
            var locator = new UnitTestServiceLocator();

            locator.NextResult = "test";
            var result = locator.GetService(typeof(string));

            Assert.AreEqual(locator.NextResult, result);
            Assert.AreEqual(typeof(string), locator.LastServiceType);
            Assert.IsNull(locator.LastKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ActivationException))]
        [ExcludeFromCodeCoverage]
        public void GetServiceWithNullResultTest()
        {
            new UnitTestServiceLocator().GetService(typeof(string));
        }
    }
}
