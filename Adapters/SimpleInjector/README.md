# README #

#### MyGet Feed ####

| Package    | develop | beta | master |
|------------|---------|------|--------|
| `Acrotech.PortableViewModel` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |

#### NuGet Feed ####

| Package | beta | master | downloads |
|---------|------|--------|-----------|
| `Acrotech.PortableViewModel` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.CommonServiceLocator.SimpleInjector) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.CommonServiceLocator.SimpleInjector) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.CommonServiceLocator.SimpleInjector.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.CommonServiceLocator.SimpleInjector) |

## About ##

This adapter was created to provide a proper universally portable `CommonServiceLocator` implementation. The official `SimpleInjector` adapter does not support profile 344 and depends upon a custom PCL build of the `CommonServiceLocator`.

## Changelog ##

#### 1.1.0 ####

* Moved to single repository

#### 1.0.0.0 ####

* Initial Release
