﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.CommonServiceLocator.SimpleInjector;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;

namespace SimpleInjector.Test
{
    [TestClass]
    public class SimpleInjectorServiceLocatorTest
    {
        [TestMethod]
        public void DefaultConstructorTest()
        {
            var adapter = new SimpleInjectorServiceLocator();

            Assert.IsNotNull(adapter.Container);
        }

        [TestMethod]
        public void ContainerOptionsConstructorTest()
        {
            var options = new ContainerOptions();
            var adapter = new SimpleInjectorServiceLocator(options);

            Assert.IsNotNull(adapter.Container);
            Assert.AreEqual(options, adapter.Container.Options);
        }

        [TestMethod]
        public void ContainerConstructorTest()
        {
            var container = new Container();
            var adapter = new SimpleInjectorServiceLocator(container);

            Assert.AreEqual(container, adapter.Container);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void NullContainerConstructorTest()
        {
            new SimpleInjectorServiceLocator((Container)null);
        }

        [TestMethod]
        public void ValidateServiceTypeTest()
        {
            var pt = new PrivateType(typeof(SimpleInjectorServiceLocator));

            // ensure no exceptions occur
            pt.InvokeStatic("ValidateServiceType", typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void ValidateServiceTypeWithNullTest()
        {
            var pt = new PrivateType(typeof(SimpleInjectorServiceLocator));

            // ensure no exceptions occur
            pt.InvokeStatic("ValidateServiceType", (Type)null);
        }

        [TestMethod]
        public void DoGetAllInstancesTest()
        {
            var adapter = new SimpleInjectorServiceLocator();
            var po = new PrivateObject(adapter);

            var items = new[] { new TestObject(1), new TestObject(2) }.OrderBy(x => x.ID).ToArray();
            adapter.Container.RegisterAll<TestObject>(items);

            var result = (IEnumerable<TestObject>)po.Invoke("DoGetAllInstances", typeof(TestObject));

            Assert.IsTrue(items.SequenceEqual(result.Cast<TestObject>().ToArray()));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void NullDoGetAllInstancesTest()
        {
            var adapter = new SimpleInjectorServiceLocator();
            var po = new PrivateObject(adapter);

            po.Invoke("DoGetAllInstances", (Type)null);

            Assert.Fail();
        }

        [TestMethod]
        public void DoGetInstanceTest()
        {
            var adapter = new SimpleInjectorServiceLocator();
            var po = new PrivateObject(adapter);

            var item = new TestObject(1);
            adapter.Container.RegisterSingle<TestObject>(item);

            var result = (TestObject)po.Invoke("DoGetInstance", typeof(TestObject), (string)null);

            Assert.AreEqual(item, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        [ExcludeFromCodeCoverage]
        public void NullDoGetInstanceTest()
        {
            var adapter = new SimpleInjectorServiceLocator();
            var po = new PrivateObject(adapter);

            po.Invoke("DoGetInstance", (Type)null, (string)null);
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        [ExcludeFromCodeCoverage]
        public void KeyedDoGetInstanceTest()
        {
            var adapter = new SimpleInjectorServiceLocator();
            var po = new PrivateObject(adapter);

            po.Invoke("DoGetInstance", typeof(string), "key");
        }
    }

    public class TestObject
    {
        public TestObject(int id)
        {
            ID = id;
        }

        public int ID { get; private set; }
    }
}
