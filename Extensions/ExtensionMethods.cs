﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.CommonServiceLocator.Extensions
{
    /// <summary>
    /// Common extensions for IServiceProvider and IServiceLocator
    /// </summary>
    public static class ExtensionMethods
    {
        private static T AbsorbActivationException<T>(Func<T> action, T defaultValue)
        {
            T result = defaultValue;

            try
            {
                result = action();
            }
            catch (ActivationException)
            {
            }

            return result;
        }

        /// <summary>
        /// Calls IServiceProvider.GetService and returns the provided <paramref name="defaultValue"/> if no registration exists
        /// </summary>
        /// <param name="source">Service provider</param>
        /// <param name="serviceType">Type of service to get</param>
        /// <param name="defaultValue">Default value to return if no registration exists</param>
        /// <returns>The registered service value or the provided <paramref name="defaultValue"/></returns>
        public static object GetServiceOrDefault(this IServiceProvider source, Type serviceType, object defaultValue = null)
        {
            return AbsorbActivationException(() => source.GetService(serviceType), defaultValue);
        }

        #region GetInstanceOrDefault

        /// <summary>
        /// Calls IServiceLocator.GetInstance and returns the provided <paramref name="defaultValue"/> if no registration exists
        /// </summary>
        /// <param name="source">Service locator</param>
        /// <param name="serviceType">Type of service to get</param>
        /// <param name="defaultValue">Default value to return if no registration exists</param>
        /// <returns>The registered service value or the provided <paramref name="defaultValue"/></returns>
        public static object GetInstanceOrDefault(this IServiceLocator source, Type serviceType, object defaultValue)
        {
            return AbsorbActivationException(() => source.GetInstance(serviceType), defaultValue);
        }

        /// <summary>
        /// Calls IServiceLocator.GetInstance and returns the provided <paramref name="defaultValue"/> if no registration exists
        /// </summary>
        /// <param name="source">Service locator</param>
        /// <param name="serviceType">Type of service to get</param>
        /// <param name="key">Registration key</param>
        /// <param name="defaultValue">Default value to return if no registration exists</param>
        /// <returns>The registered service value or the provided <paramref name="defaultValue"/></returns>
        public static object GetKeyedInstanceOrDefault(this IServiceLocator source, Type serviceType, string key, object defaultValue = null)
        {
            return AbsorbActivationException(() => source.GetInstance(serviceType, key), defaultValue);
        }

        /// <summary>
        /// Calls IServiceLocator.GetInstance and returns the provided <paramref name="defaultValue"/> if no registration exists
        /// </summary>
        /// <typeparam name="T">Type of service to get</typeparam>
        /// <param name="source">Service locator</param>
        /// <param name="defaultValue">Default value to return if no registration exists</param>
        /// <returns>The registered service value or the provided <paramref name="defaultValue"/></returns>
        public static T GetInstanceOrDefault<T>(this IServiceLocator source, T defaultValue = default(T))
        {
            return AbsorbActivationException(() => source.GetInstance<T>(), defaultValue);
        }

        /// <summary>
        /// Calls IServiceLocator.GetInstance and returns the provided <paramref name="defaultValue"/> if no registration exists
        /// </summary>
        /// <typeparam name="T">Type of service to get</typeparam>
        /// <param name="source">Service locator</param>
        /// <param name="key">Registration key</param>
        /// <param name="defaultValue">Default value to return if no registration exists</param>
        /// <returns>The registered service value or the provided <paramref name="defaultValue"/></returns>
        public static T GetKeyedInstanceOrDefault<T>(this IServiceLocator source, string key, T defaultValue = default(T))
        {
            return AbsorbActivationException(() => source.GetInstance<T>(key), defaultValue);
        }

        #endregion
    }
}
