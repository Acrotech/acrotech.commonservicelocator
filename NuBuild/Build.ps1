# Required Function Implementations
# =================================
# function Log([string]$message) :: VOID
# function OnError([string]$message, [int]$code) :: VOID

function Log-Build([string]$message)
{
  Log "[NuBuild] $message"
}

function OnError-Exit([string]$message)
{
  if ($LASTEXITCODE -and $LASTEXITCODE -ne 0)
  {
    OnError "[!ERROR!] $message" $LASTEXITCODE
  }
  elseif ($Error.Count -gt 0)
  {
    OnError "[!ERROR!] $message" 0
  }
}

function Get-ValueOrDefault([string]$value, [string]$defaultValue = "")
{
  if ([string]::IsNullOrEmpty($value)) { $defaultValue } else { $value }
}

function Get-FormatOrDefault([string]$format, [string]$value, [string]$defaultValue = "")
{
  if ([string]::IsNullOrEmpty($value)) { $defaultValue } else { [string]::Format($format, $value) }
}

function Get-Flag([string]$flag)
{
  ![string]::IsNullOrEmpty($flag)
}

function Get-Path([string]$relativePath)
{
  Join-Path $env:SourcesPath $relativePath
}

function Add-ParamValue([string[]]$params, [string]$key, [string]$value)
{
  if (![string]::IsNullOrEmpty($value)) { $params += @($key, $value) }
  return $params
}

function Add-ParamFormat([string[]]$params, [string]$format, [string]$value)
{
  if (![string]::IsNullOrEmpty($value))
  {
    $list = $format.Split(' ', [StringSplitOptions]::RemoveEmptyEntries)
    ForEach ($param in $list)
    {
      $params += [string]::Format($param, $value)
    }
  }

  return $params
}

function Add-Param([string[]]$params, [string]$value)
{
  if (![string]::IsNullOrEmpty($value)) { $params += $value }
  return $params
}

function Add-ParamList([string[]]$params, [string]$value)
{
  if (![string]::IsNullOrEmpty($value)) { $params += $value.Split(' ', [StringSplitOptions]::RemoveEmptyEntries) }
  return $params
}

function Run-Command([string]$cmd, [string[]]$params, [string]$onFailedMessage = "Command Failed")
{
  $Error.Clear()

  Log-Build "$cmd $params"

  & $cmd $params

  OnError-Exit $onFailedMessage
}

function Initialize-Environment()
{
  if ($MyInvocation.MyCommand.Path)
  {
    $env:NuBuildRootPath = Get-ValueOrDefault $env:NuBuildRootPath (Split-Path -Path $MyInvocation.MyCommand.Path)
  }

  $env:SourcesPath = Get-ValueOrDefault $env:SourcesPath (Join-Path $env:NuBuildRootPath "..")
  $env:VisualStudioVersion = Get-ValueOrDefault $env:VisualStudioVersion "12.0"

  $env:MsBuildExe = Get-ValueOrDefault $env:MsBuildExe (Join-Path $env:SystemRoot "Microsoft.NET\Framework\v4.0.30319\MSBuild.exe")
  $env:TextTransformExe = Get-ValueOrDefault $env:TextTransformExe (Join-Path ${env:CommonProgramFiles(x86)} "microsoft shared\TextTemplating\$env:VisualStudioVersion\TextTransform.exe")
  $env:VsTestConsoleExe = Get-ValueOrDefault $env:VsTestConsoleExe (Join-Path ${env:ProgramFiles(x86)} "Microsoft Visual Studio $env:VisualStudioVersion\Common7\IDE\CommonExtensions\Microsoft\TestWindow\VsTest.Console.exe")
  $env:NuGetExe = Get-ValueOrDefault $env:NuGetExe (Get-Path ".nuget\NuGet.exe")

  $env:NuGetOutDir = Get-ValueOrDefault $env:NuGetOutDir (Join-Path $env:NuBuildRootPath "nupkg")
  $env:Targets = Get-ValueOrDefault $env:Targets "Rebuild"
  $env:Configuration = Get-ValueOrDefault $env:Configuration "Release"
  $env:Platform = Get-ValueOrDefault $env:Platform "AnyCPU"

  $env:DefaultNuGetRestoreArgs = Get-ValueOrDefault $env:DefaultNuGetRestoreArgs "-NoCache -NonInteractive"
  $env:DefaultNuGetVerbosity = Get-ValueOrDefault $env:DefaultNuGetVerbosity "Detailed"
  $env:DefaultBuildPropertiesPath = Get-ValueOrDefault $env:DefaultBuildPropertiesPath "Properties\build.properties"
  $env:DefaultVersionPropertiesPath = Get-ValueOrDefault $env:DefaultVersionPropertiesPath "Properties\version.properties"
  $env:DefaultVersionDetailsPath = Get-ValueOrDefault $env:DefaultVersionDetailsPath "Properties\VersionDetails.tt"
  $env:DefaultPackagesFile = Get-ValueOrDefault $env:DefaultPackagesFile "packages.config"
  $env:DefaultTransformArgs = Get-ValueOrDefault $env:DefaultTransformArgs ""
  $env:DefaultBuildArgs = Get-ValueOrDefault $env:DefaultBuildArgs ""
  $env:DefaultTestArgs = Get-ValueOrDefault $env:DefaultTestArgs "/InIsolation /Enablecodecoverage"
  $env:DefaultNuGetPackArgs = Get-ValueOrDefault $env:DefaultNuGetPackArgs "-NonInteractive -NoPackageAnalysis -IncludeReferencedProjects"

  if (Test-Path $env:NuGetOutDir)
  {
    Log-Build "Deleting Old NuGet packages ($env:NuGetOutDir\*.nupkg)"
    Remove-Item (Join-Path $env:NuGetOutDir "*.nupkg") -Recurse
  }
  else
  {
    Log-Build "Creating NuGet packages directory ($env:NuGetOutDir)"
    New-Item -ItemType Directory -Path $env:NuGetOutDir | Out-Null
  }

  Log-Build @"

NuBuild Environment:
NuBuildRootPath               = $env:NuBuildRootPath
SourcesPath                   = $env:SourcesPath
VisualStudioVersion           = $env:VisualStudioVersion

MsBuildExe                    = $env:MsBuildExe
TextTransformExe              = $env:TextTransformExe
VsTestConsoleExe              = $env:VsTestConsoleExe
NuGetExe                      = $env:NuGetExe

NuGetOutDir                   = $env:NuGetOutDir
Targets                       = $env:Targets
Configuration                 = $env:Configuration
Platform                      = $env:Platform
OverrideVersion               = $env:OverrideVersion

DefaultNuGetRestoreArgs       = $env:DefaultNuGetRestoreArgs
DefaultNuGetVerbosity         = $env:DefaultNuGetVerbosity
DefaultBuildPropertiesPath    = $env:DefaultBuildPropertiesPath
DefaultVersionPropertiesPath  = $env:DefaultVersionPropertiesPath
DefaultVersionDetailsPath     = $env:DefaultVersionDetailsPath
DefaultPackagesFile           = $env:DefaultPackagesFile
DefaultTransformArgs          = $env:DefaultTransformArgs
DefaultBuildArgs              = $env:DefaultBuildArgs
DefaultTestArgs               = $env:DefaultTestArgs
DefaultNuGetPackArgs          = $env:DefaultNuGetPackArgs

EnableGitVersionOverride      = $env:EnableGitVersionOverride
EnableVersionPropertiesPrep   = $env:EnableVersionPropertiesPrep

"@
}

function Prep-Project(
  [string]$basePath,
  [string]$buildPath = $env:DefaultBuildPropertiesPath,
  [string]$versionPath = $env:DefaultVersionPropertiesPath
)
{
  if ($buildPath)
  {
    $path = Join-Path $basePath $buildPath

    if (Test-Path $path)
    {
      Log-Build "Prepping $path"

      [string]::Format("# Generated {0}", (Get-Date -Format u))                               | Out-File -FilePath $path
      [string]::Format("Branch = {0}",    (Get-ValueOrDefault $env:GitVersion_BranchName))    | Out-File -FilePath $path -Append
      [string]::Format("Commit = {0}",    (Get-ValueOrDefault $env:GitVersion_Sha))           | Out-File -FilePath $path -Append
      [string]::Format("Number = {0}",    (Get-ValueOrDefault $env:BuildCounter))             | Out-File -FilePath $path -Append
      [string]::Format("Tag = {0}",       (Get-ValueOrDefault $env:GitVersion_PreReleaseTag)) | Out-File -FilePath $path -Append
      [string]::Format("MetaData = {0}",  (Get-ValueOrDefault $env:GitVersion_BuildMetaData)) | Out-File -FilePath $path -Append

      Get-Content $path
    }
  }

  if ($versionPath -and (Get-Flag $env:EnableVersionPropertiesPrep))
  {
    $path = Join-Path $basePath $versionPath

    if (Test-Path $path)
    {
      Log-Build "Prepping $path"

      [string]::Format("# Generated {0}", (Get-Date -Format u))                               | Out-File -FilePath $path
      [string]::Format("Major = {0}",     (Get-ValueOrDefault $env:GitVersion_Major))         | Out-File -FilePath $path -Append
      [string]::Format("Minor = {0}",     (Get-ValueOrDefault $env:GitVersion_Minor))         | Out-File -FilePath $path -Append
      [string]::Format("Patch = {0}",     (Get-ValueOrDefault $env:GitVersion_Patch))         | Out-File -FilePath $path -Append

      Get-Content $path
    }
  }
}

function Build(
  [string]$basePath,
  [string]$projectFileName,
  [string]$packagesFileName = $env:DefaultPackagesFile,
  [string]$restoreArgs = $env:DefaultNuGetRestoreArgs,
  [string]$versionDetailsPath = $env:DefaultVersionDetailsPath,
  [string]$transformArgs = $env:DefaultTransformArgs,
  [string]$buildArgs = $env:DefaultBuildArgs
)
{
  Restore-Packages (Join-Path $basePath $packagesFileName) $env:SourcesPath $restoreArgs

  Transform-Project-Version $basePath $versionDetailsPath $transformArgs

  Build-Project (Join-Path $basePath $projectFileName) $buildArgs
}

function Restore-Packages(
  [string]$packagesPath,
  [string]$solutionDirectory = $env:SourcesPath,
  [string]$restoreArgs = $env:DefaultNuGetRestoreArgs
)
{
  if (Test-Path $packagesPath)
  {
    Log-Build "Restoring Packages for $packagesPath"

    $params = @("restore", $packagesPath)
    $params = Add-ParamValue $params "-SolutionDirectory" $solutionDirectory
    $params = Add-ParamValue $params "-Verbosity" $env:DefaultNuGetVerbosity
    $params = Add-ParamList $params $restoreArgs

    Run-Command $env:NuGetExe $params "Restore Packages Failed"
  }
}

function Transform-Project-Version(
  [string]$basePath,
  [string]$versionDetailsPath = $env:DefaultVersionDetailsPath,
  [string]$transformArgs = $env:DefaultTransformArgs
)
{
  $path = Join-Path $basepath $versionDetailsPath

  if (Test-Path $path)
  {
    Log-Build "Transforming $path"

    $params = Add-ParamList @() $transformArgs
    $params = Add-Param $path

    Run-Command $env:TextTransformExe $params "Transform Failed"
  }
}

function Build-Project(
  [string]$projectPath,
  [string]$buildArgs = $env:DefaultBuildArgs
)
{
  if (Test-Path $projectPath)
  {
    Log-Build "Building $projectPath"

    $params = @($projectPath, "/t:$env:Targets", "/p:Configuration=$env:Configuration", "/p:Platform=$env:Platform")
    $params = Add-ParamList $params $buildArgs

    Run-Command $env:MsBuildExe $params "MSBuild Failed"
  }
}

function Run-Tests(
  [string]$assemblyPath,
  [string]$settingsPath = $null,
  [string]$testArgs = $env:DefaultTestArgs
)
{
  if (Test-Path $assemblyPath)
  {
    Log-Build "Testing $assemblyPath"

    $params = @($assemblyPath)
    $params = Add-ParamFormat $params "/Settings:{0}" $settingsPath
    $params = Add-ParamList $params $testArgs

    Run-Command $env:VsTestConsoleExe $params "VsTest.Console Failed"
  }
}

function Pack-Packagable(
  [string]$packagablePath,
  [string]$packArgs = $env:DefaultNuGetPackArgs
)
{
  if (Test-Path $packagablePath)
  {
    Log-Build "Packing $packagablePath"

    $params = @("Pack", $packagablePath, "-OutputDirectory", $env:NuGetOutDir)
    $params = Add-ParamValue $params "-Verbosity" $env:Verbosity
    $params = Add-ParamFormat $params "-Prop Configuration={0}" $env:Configuration
    $params = Add-ParamValue $params "-Version" $env:OverrideVersion
    $params = Add-ParamList $params $packArgs

    Run-Command $env:NuGetExe $params "NuGet Pack Failed"
  }
}

function NuBuild-Discover([string]$scriptPattern = "^NuBuild(\.[\w-]*[0-9]+)?\.(ps1|bat|cmd)$")
{
  $files = @(Get-ChildItem -Path $env:SourcesPath -Recurse | Where-Object { $_.Name -Match $scriptPattern }) | Sort-Object { $_.Name }

  ForEach ($file in $files)
  {
    Log-Build "Running Build Script: $($file.FullName)"

    & $file.FullName
    OnError-Exit "Build Script Failed"
  }
}

function NuBuild-Inject()
{
  if (![string]::IsNullOrEmpty($env:PrepIncludes))
  {
    ForEach ($path in $env:PrepIncludes.Split(','))
    {
      Log-Build "Injecting Prep: $path"

      Prep-Project (Get-Path $path)
    }
  }

  if (![string]::IsNullOrEmpty($env:RestoreIncludes))
  {
    ForEach ($packagesPath in $env:RestoreIncludes.Split(','))
    {
      Log-Build "Injecting Prep: $packagesPath"

      Restore-Packages (Get-Path $packagesPath)
    }
  }

  if (![string]::IsNullOrEmpty($env:TransformIncludes))
  {
    ForEach ($path in $env:TransformIncludes.Split(','))
    {
      Log-Build "Injecting Transform: $path"

      Transform-Project-Version (Get-Path $path)
    }
  }

  if (![string]::IsNullOrEmpty($env:BuildIncludes))
  {
    ForEach ($projectPath in $env:BuildIncludes.Split(','))
    {
      Log-Build "Injecting Build: $projectPath"

      Build-Project (Get-Path $projectPath)
    }
  }

  if (![string]::IsNullOrEmpty($env:TestIncludes))
  {
    ForEach ($assemblyPath in $env:TestIncludes.Split(','))
    {
      Log-Build "Injecting Test: $assemblyPath"

      Run-Tests (Get-Path $assemblyPath)
    }
  }

  if (![string]::IsNullOrEmpty($env:PackIncludes))
  {
    ForEach ($packagablePath in $env:PackIncludes.Split(','))
    {
      Log-Build "Injecting Pack: $packagablePath"

      Pack-Packagable (Get-Path $packagablePath)
    }
  }
}

# initialize required environment variables
Initialize-Environment

#Get-ChildItem Env: | Sort-Object Name | Select-Object Name, Value

# check if we should force using the GitVersion_NuGetVersion value
if (Get-Flag $env:EnableGitVersionOverride)
{
  $env:OverrideVersion = $env:GitVersion_NuGetVersion
  Log-Build "Overriding Version with NuGet Version: $env:OverrideVersion"
}

# call any build scripts discovered within the SourcesPath
NuBuild-Discover

# call any build functions with injected variables
NuBuild-Inject

Log-Build "Complete"
