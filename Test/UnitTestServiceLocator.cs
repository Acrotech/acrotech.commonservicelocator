﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.CommonServiceLocator.Test
{
    public class UnitTestServiceLocator : IServiceLocator
    {
        public object NextResult { get; set; }

        public Type LastServiceType { get; private set; }
        public string LastKey { get; private set; }

        public void ThrowActivationExceptionIfNullResult()
        {
            if (NextResult == null)
            {
                throw new ActivationException();
            }
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            LastServiceType = typeof(TService);
            LastKey = null;

            ThrowActivationExceptionIfNullResult();

            return (IEnumerable<TService>)NextResult;
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            LastServiceType = serviceType;
            LastKey = null;

            ThrowActivationExceptionIfNullResult();

            return (IEnumerable<object>)NextResult;
        }

        public TService GetInstance<TService>(string key)
        {
            LastServiceType = typeof(TService);
            LastKey = key;

            ThrowActivationExceptionIfNullResult();

            return (TService)NextResult;
        }

        public TService GetInstance<TService>()
        {
            LastServiceType = typeof(TService);
            LastKey = null;

            ThrowActivationExceptionIfNullResult();

            return (TService)NextResult;
        }

        public object GetInstance(Type serviceType, string key)
        {
            LastServiceType = serviceType;
            LastKey = key;

            ThrowActivationExceptionIfNullResult();

            return NextResult;
        }

        public object GetInstance(Type serviceType)
        {
            LastServiceType = serviceType;
            LastKey = null;

            ThrowActivationExceptionIfNullResult();

            return NextResult;
        }

        public object GetService(Type serviceType)
        {
            LastServiceType = serviceType;
            LastKey = null;

            ThrowActivationExceptionIfNullResult();

            return NextResult;
        }
    }
}
