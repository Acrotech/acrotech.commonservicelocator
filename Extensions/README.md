# README #

#### MyGet Feed ####

| Package    | develop | beta | master |
|------------|---------|------|--------|
| `Acrotech.CommonServiceLocator.Extensions` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |

#### NuGet Feed ####

| Package | beta | master | downloads |
|---------|------|--------|-----------|
| `Acrotech.CommonServiceLocator.Extensions` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.CommonServiceLocator.Extensions) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.CommonServiceLocator.Extensions) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.CommonServiceLocator.Extensions.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.CommonServiceLocator.Extensions) |

## About ##

This library provides some universally portable CommonServiceLocator extensions to simplify certain common operations.

* **GetServiceOrDefault**: allows returning a default value if no registration exists (for all IServiceProvider implementations)
* **GetInstanceOrDefault**: allows returning a default value if no registration exists (for IServiceLocator implementations)

## Usage Notes ##

Because all other default values are optional parameters, the **NON**-generic extension `GetInstanceOrDefault` cannot have a default parameter as it creates an extension method signature collision with `GetInstanceOrDefault<T>`.

## Changelog ##

#### 1.1.0 ####

* Moved to single repository

#### 1.0.0.0 ####

* Initial Release
