﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.CommonServiceLocator.Extensions;
using Microsoft.Practices.ServiceLocation;
using System.Reflection;
using System.Diagnostics.CodeAnalysis;

namespace Acrotech.CommonServiceLocator.Test
{
    [TestClass]
    public class ExtensionMethodsTest
    {
        [TestMethod]
        public void AbsorbActivationExceptionTest()
        {
            var pt = new PrivateType(typeof(ExtensionMethods));

            var paramTypes = pt.ReferencedType.GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                .Where(x => x.Name == "AbsorbActivationException")
                .Single()
                .GetParameters()
                .Select(x => x.ParameterType)
                .ToArray();
            var typeArgs = new[] { typeof(string) };

            Func<string> action = () => { return "test"; };

            var result = pt.InvokeStatic("AbsorbActivationException", paramTypes, new object[] { action, "default" }, typeArgs);
            Assert.AreEqual("test", result);

            action = () => { throw new ActivationException(); };
            result = pt.InvokeStatic("AbsorbActivationException", paramTypes, new object[] { action, "default" }, typeArgs);
            Assert.AreEqual("default", result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void AbsorbActivationExceptionOtherExceptionTest()
        {
            var pt = new PrivateType(typeof(ExtensionMethods));

            var paramTypes = pt.ReferencedType.GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                .Where(x => x.Name == "AbsorbActivationException")
                .Single()
                .GetParameters()
                .Select(x => x.ParameterType)
                .ToArray();
            var typeArgs = new[] { typeof(string) };

            Func<string> action = () => { throw new InvalidOperationException(); };

            pt.InvokeStatic("AbsorbActivationException", paramTypes, new object[] { action, "default" }, typeArgs);
        }

        [TestMethod]
        public void GetServiceOrDefaultTest()
        {
            var locator = new UnitTestServiceLocator();

            Assert.IsNull(locator.GetServiceOrDefault(typeof(string)));
            Assert.AreEqual("default", locator.GetServiceOrDefault(typeof(string), "default"));

            locator.NextResult = "test";
            Assert.AreEqual("test", locator.GetServiceOrDefault(typeof(string)));
            Assert.AreEqual("test", locator.GetServiceOrDefault(typeof(string), "default"));
        }

        [TestMethod]
        public void GetInstanceOrDefaultTest()
        {
            var locator = new UnitTestServiceLocator();

            Assert.IsNull(locator.GetInstanceOrDefault(typeof(string), null));
            Assert.AreEqual("default", locator.GetInstanceOrDefault(typeof(string), "default"));

            locator.NextResult = "test";
            Assert.AreEqual("test", locator.GetInstanceOrDefault(typeof(string), null));
            Assert.AreEqual("test", locator.GetInstanceOrDefault(typeof(string), "default"));
        }

        [TestMethod]
        public void GetKeyedInstanceOrDefaultTest()
        {
            var locator = new UnitTestServiceLocator();

            Assert.IsNull(locator.GetKeyedInstanceOrDefault(typeof(string), "key"));
            Assert.AreEqual("default", locator.GetKeyedInstanceOrDefault(typeof(string), "key", "default"));

            locator.NextResult = "test";
            Assert.AreEqual("test", locator.GetKeyedInstanceOrDefault(typeof(string), "key"));
            Assert.AreEqual("test", locator.GetKeyedInstanceOrDefault(typeof(string), "key", "default"));
        }

        [TestMethod]
        public void GenericGetInstanceOrDefaultTest()
        {
            var locator = new UnitTestServiceLocator();

            Assert.IsNull(locator.GetInstanceOrDefault<string>());
            Assert.AreEqual("default", locator.GetInstanceOrDefault<string>("default"));

            locator.NextResult = "test";
            Assert.AreEqual("test", locator.GetInstanceOrDefault<string>());
            Assert.AreEqual("test", locator.GetInstanceOrDefault<string>("default"));
        }

        [TestMethod]
        public void GenericGetKeyedInstanceOrDefaultTest()
        {
            var locator = new UnitTestServiceLocator();

            Assert.IsNull(locator.GetKeyedInstanceOrDefault<string>("key"));
            Assert.AreEqual("default", locator.GetKeyedInstanceOrDefault<string>("key", "default"));

            locator.NextResult = "test";
            Assert.AreEqual("test", locator.GetKeyedInstanceOrDefault<string>("key"));
            Assert.AreEqual("test", locator.GetKeyedInstanceOrDefault<string>("key", "default"));
        }
    }
}
