@ECHO OFF

SET PKG=%1

IF NOT "%PKG%" == "" (
	ECHO Using Command Line Parameter: %PKG%
	GOTO PROMPT
)

FOR /R %%F in (*.nupkg) DO SET PKG=%%~nxF

IF "%PKG%" == "" (
	ECHO No NuGet Package Found?
	GOTO END
)

:PROMPT
SET /P PUSH=Push %PKG% [y/N]? 

IF "%PUSH%" == "y" SET PUSH=Y

IF "%PUSH%" == "Y" (
	ECHO Pushing %PKG% ...
	..\..\.nuget\NuGet.exe Push %PKG%
)

:END
PAUSE
