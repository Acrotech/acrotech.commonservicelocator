﻿using Microsoft.Practices.ServiceLocation;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acrotech.CommonServiceLocator.SimpleInjector
{
    /// <summary>
    /// Simple Injector implementation of the Microsoft CommonServiceLocator
    /// </summary>
    public class SimpleInjectorServiceLocator : ServiceLocatorImplBase
    {
        /// <summary>
        /// Creates a new adapter with a default container
        /// </summary>
        public SimpleInjectorServiceLocator()
            : this(new Container())
        {
        }

        /// <summary>
        /// Creates a new adapter with a new container using the provided options
        /// </summary>
        /// <param name="options">Options to initialize the container with</param>
        public SimpleInjectorServiceLocator(ContainerOptions options)
            : this(new Container(options))
        {
        }

        /// <summary>
        /// Creates a new adapter with the provided container
        /// </summary>
        /// <param name="container">Container to adapt</param>
        public SimpleInjectorServiceLocator(Container container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            Container = container;
        }

        /// <summary>
        /// Simple Injector Container
        /// </summary>
        public Container Container { get; private set; }

        private static void ValidateServiceType(Type serviceType)
        {
            if (serviceType == null)
            {
                throw new ArgumentNullException("serviceType");
            }
        }

        /// <inheritdoc/>
        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            ValidateServiceType(serviceType);

            return Container.GetAllInstances(serviceType);
        }

        /// <inheritdoc/>
        protected override object DoGetInstance(Type serviceType, string key)
        {
            ValidateServiceType(serviceType);

            if (string.IsNullOrEmpty(key))
            {
                return Container.GetInstance(serviceType);
            }
            else
            {
                throw new NotSupportedException("Keyed registration is not supported by Simple Injector");
            }
        }
    }
}
